import unittest
from unittest import mock

import ioe.ocpp.client as ocpp


class OCPPTestCase(unittest.TestCase):

    @mock.patch('ioe.ocpp.client.websocket')
    def setUp(self, mock_ws):
        self.charging_station = ocpp.OCPPConnection('ws://csms.example.com')
        mock_ws.create_connection.assert_called_with('ws://csms.example.com')

    @mock.patch('ioe.ocpp.client.websocket')
    def test_ocpp(self, mock_ws):
        self.charging_station.request('BootNotification',
                                      {'reason': 'PowerUp',
                                       'chargingStation': {
                                           'model': '1',
                                           'vendorName': 'a'}})
        self.charging_station.ws.send.assert_called_with(
            '[2, "1", "BootNotification", ' +
            '{"reason": "PowerUp", "chargingStation": ' +
            '{"model": "1", "vendorName": "a"}}]')
        self.charging_station.ws.recv.assert_called_once()


if __name__ == '__main__':
    unittest.main()
