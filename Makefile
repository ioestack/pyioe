# This will output the help for each task
.PHONY: help
help: ## Print this help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

.PHONY: test
test: ## Run tests.
	tox

.PHONY: dev
dev: ## Development environment.
	python setup.py develop

.PHONY: serve
serve: ## Run development server.
	python -m ioe.ocpp.server

.PHONY: publish
publish: ## Update pypi.org
	rm -fr build/*
	rm -fr dist/*
	python setup.py sdist bdist_wheel
	twine upload dist/*

.PHONY: doc
doc:
	cd docs && $(MAKE) html
