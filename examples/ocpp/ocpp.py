#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# OCPP Session Example
#
# Start a server before:
# $ python3 -m ioe.ocpp.server
#
# Run example client:
# $ ./example/ocpp/ocpp.py
#

import ioe.ocpp.client as ocpp


def main():
    charging_station = ocpp.OCPPConnection('ws://localhost:8765/')
    print(charging_station.request('BootNotification'))


if __name__ == '__main__':
    main()
