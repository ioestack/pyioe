import json
import os
import websocket
from jsonschema import validate


class OCPPConnection():
        def __init__(self, url):
            self.url = url
            self.ws = websocket.create_connection(url)
            self.message_id = 0
            self.message_type_id_call = 2

            self.schemas_dict = {}

            schemas_dir = os.path.join(os.path.dirname(__file__),
                                       'OCPP-2.0_part3_schemas/')
            schemas = os.listdir(schemas_dir)

            for schema in schemas:
                    self.schemas_dict[schema] = json.load(
                            open(os.path.join(schemas_dir, schema),
                                 encoding='utf-8-sig'))

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_value, traceback):
            self.ws.close()

        def request(self, action, payload):
            validate(payload, self.schemas_dict[action + 'Request_v1p0.json'])

            self.message_id += 1

            self.ws.send(json.dumps([self.message_type_id_call,
                                     str(self.message_id),
                                     str(action),
                                     payload]))
            return self.ws.recv()
