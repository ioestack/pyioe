.. ioe documentation master file, created by
   sphinx-quickstart on Wed Sep 19 01:01:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Internet of Energy!
==================================

The Python Internet of Energy Library.

Boosting the Internet of Things for the Energy Industry with data structures, algorithms and communications protocols implemented in Python.

.. toctree::
   :maxdepth: 1

   introduction
   electric-vehicle
